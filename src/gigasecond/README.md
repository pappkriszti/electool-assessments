###Description
Calculate the moment when someone has lived for 10^9 seconds.

A gigasecond is 10^9 (1,000,000,000) seconds.

###Guidelines
- Fork this project.
- Create a plain solution on your language
- If your language provides a method in the standard library that does this look-up, pretend it doesn't exist and implement it yourself.
- Write tests
- Commit the important milestones and not just the final result
