package gigasecond;

public class Gigasecond {
    public static void main(String[] args) {
        int gigasecond = 1000000000;
        System.out.println(howManyYearsEtc(gigasecond));
    }

    public static String howManyYearsEtc(int inputSeconds){
        int year = 31556926;
        int month = year / 12;
        int day = year / 365;
        int hour = 60 * 60;
        int minute = 60;

        return inputSeconds/year + " Years, "
                + (inputSeconds % year)/month + " Months, "
                + (inputSeconds % month)/day + " Days, "
                + (inputSeconds % day)/hour + " Hours, "
                + (inputSeconds % hour)/minute + " Minutes, "
                + (inputSeconds % minute) + " Seconds.";
    }
}
