package leapyear;

public class Leap {
    public static void main(String[] args) {
        System.out.println(isALeap(1997));
        System.out.println(isALeap(1996));
    }

    public static boolean isALeap(int year){
        if (year % 4 != 0) {
            return false;
        } else if (year % 400 == 0) {
            return true;
        } else if (year % 100 == 0) {
            return false;
        } else {
            return true;
        }
    }
}
