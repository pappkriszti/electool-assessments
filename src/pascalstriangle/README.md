###Description
Compute Pascal's triangle up to a given number of rows.

In Pascal's Triangle each number is computed by adding the numbers to the right and left of the current position in the previous row.

```
1.     1
2.    1 1
3.   1 2 1
4.  1 3 3 1
5. 1 4 6 4 1
6. # ... etc
```

###Guidelines
- Fork this project.
- Create a plain solution on your language
- If your language provides a method in the standard library that does this look-up, pretend it doesn't exist and implement it yourself.
- Write tests
- Commit the important milestones and not just the final result
