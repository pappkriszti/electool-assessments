package pascalstriangle;

public class PascalsTraingle {

    public static void main(String[] args) {
        draw(6);
    }

    public static void draw(int size){
        for(int i = 0; i < size; i++){
            int number = 1;
            System.out.format("%" + (size-i) * 2 + "s","");

            for(int j = 0; j <= i; j++) {
                System.out.format("%4d",number);
                number = number * (i - j) / (j + 1);
            }
            System.out.println();
        }
    }
}
