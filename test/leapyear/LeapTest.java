package leapyear;

import org.junit.Assert;
import org.junit.Test;
import static leapyear.Leap.isALeap;

public class LeapTest {

    @Test
    public void itsNot() throws Exception {
        Assert.assertTrue(isALeap(1996));
    }

    @Test
    public void itIs() throws Exception {
        Assert.assertFalse(isALeap(1997));
    }
}