package gigasecond;

import org.junit.Assert;
import org.junit.Test;

import static gigasecond.Gigasecond.howManyYearsEtc;

public class GigasecondTest {

    @Test
    public void testOneGigasecond() throws Exception {
        Assert.assertEquals("31 Years, 8 Months, 8 Days, 10 Hours, 46 Minutes, 40 Seconds.", howManyYearsEtc(1000000000));
    }

    @Test
    public void testTwoJustMinutes() throws Exception {
        Assert.assertEquals("0 Years, 0 Months, 0 Days, 0 Hours, 1 Minutes, 10 Seconds.", howManyYearsEtc(70));
    }

}